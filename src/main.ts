import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { store, key } from "./store";

import { VuesticPlugin } from "vuestic-ui";

const app = createApp(App);
app.use(store, key);
app.use(router);

app.use(VuesticPlugin, {
  colors: {},
  componentsAll: {
    dark: true,
  },
});
app.mount("#app");
