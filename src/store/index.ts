import { createStore, Store } from "vuex";
import { InjectionKey } from "vue";

export interface State {
  index: number | null;
  timer: number;
}

export const key: InjectionKey<Store<State>> = Symbol();

const DEFAULT_TIMER = 10;

const DECREMENT = "decrement";
// const INCREMENT = "increment";
const START = "start";
const STOP = "stop";

export const store = createStore<State>({
  state: {
    index: null,
    timer: DEFAULT_TIMER,
  },
  getters: {
    isRunning: (state) => state.index,
    timer: (state) => state.timer,
  },
  mutations: {
    /**
     * Increment the value of the timer.
     * @param state current state.
     */
    increment(state) {
      state.timer = state.timer + 1;
    },
    /**
     * Decrement the value of the timer.
     * @param state current state.
     */
    decrement(state) {
      const canDecrementTimer = state.timer > 0;
      if (canDecrementTimer) {
        state.timer = state.timer - 1;
      }
    },
    /**
     * Set the timer value to specified value.
     * @param state current state.
     * @param time new value of the timer. Must be more than 0.
     */
    setTimer(state, time) {
      const canSetTimer = state.timer > 0;
      if (canSetTimer) {
        state.timer = time;
      }
    },
    /**
     * Set the current interval index to given value.
     * @param state current state.
     * @param i interval index.
     */
    start(state, i) {
      state.index = i;
    },
    /**
     * Invalid the current interval index.
     * @param state current state.
     */
    stop(state) {
      state.index = null;
    },
  },
  actions: {
    reset({ commit, getters }) {
      const isNotRunning = !getters.isRunning;
      if (isNotRunning) {
        commit("setTimer", 0);
      }
    },
    start({ commit, dispatch, getters }) {
      const validTimer = getters.timer > 0;
      const isNotRunning = !getters.isRunning;
      const canStartTimer = isNotRunning && validTimer;
      if (canStartTimer) {
        // i interval will execute a tick of timer each second.
        const i = setInterval(() => {
          // TODO: Calculate the difference in timespan.
          commit(DECREMENT);
          if (getters.timer < 1) {
            dispatch(STOP);
          }
        }, 1000);
        commit(START, i);
      }
    },
    /**
     * Stop the timer from running if it's running.
     * @param context store context.
     */
    stop(context) {
      const couldStop = context.getters.isRunning;
      if (couldStop) {
        clearInterval(couldStop);
        context.commit(STOP);
      }
    },
    /**
     * Toggle running state of the timer.
     * @param context store context.
     */
    toggle(context) {
      const couldStop = context.getters.isRunning;
      if (couldStop) {
        context.dispatch(STOP);
      } else {
        context.dispatch(START);
      }
    },
  },
  modules: {},
  strict: process.env.NODE_ENV !== "production",
});
